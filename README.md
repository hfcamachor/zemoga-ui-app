## Héctor Camacho - Zemoga UI test.

This is a test for zemoga by Héctor Camacho - hectorfernandocr@gmail.com

## Available Scripts

In the project directory, you can run:

### `npm install`

After cloning this repository you should nav to the project folder and run npm install. Make sure you have the latest node and npm version.

#### `npm start`

In order to see the app you need to run npm start, this runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

###### License

This project was made with Create React App which is an open source software licensed as MIT.