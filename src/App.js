import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import './App.css';
// @Components
import MainPage from './components/MainPage/MainPage'; 
import NavBar from './components/NavBar/NavBar';
import Footer from './components/Footer/Footer';
import MockComponentOne from './components/MockComponents/MockComponents1';
import MockComponentOTwo from './components/MockComponents/MockComponents2';
import MockComponentOThree from './components/MockComponents/MockComponents3';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <NavBar />
          <Route path="/" exact component={MainPage} />
          <Route path="/past-trials" exact component={MockComponentOne} />
          <Route path="/how-it-works" exact component={MockComponentOTwo} />
          <Route path="/log-in-sign-up" exact component={MockComponentOThree} />
        <Footer />
      </div>
    </BrowserRouter>
  );
}

export default App;
