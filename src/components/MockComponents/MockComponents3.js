import './MockComponents.scss';
import React from 'react'

const MockComponentOThree = () => (
    <div className="mockComponents">
        <h1>
            Log In / Sign Up
        </h1>
    </div>
)

export default MockComponentOThree;