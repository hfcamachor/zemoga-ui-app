import './MockComponents.scss';
import React from 'react'

const MockComponentOne = () => (
    <div className="mockComponents">
        <h1>
            Past Trials
        </h1>
    </div>
)

export default MockComponentOne;