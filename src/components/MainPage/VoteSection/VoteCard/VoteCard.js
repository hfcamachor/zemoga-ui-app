import './VoteCard.scss';

import React, { Component } from 'react';

// @components
import VoteUpDownThumb from './VoteUpDownThumb/VoteUpDownThumb';
import VoteBar from './VoteBar/VoteBar';
import TransparentButton from 'components/commons/TransparentButton/TransparentButton';


// @assets
const images = require.context('assets/', true);

class VoteCard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            voteSelected: '',
            voteCount: 50,
            voteDone: false
        }

        this.handleVoteButton = this.handleVoteButton.bind(this);
    }

    componentDidMount() {
        const { voteCardContent } = this.props;
        const storageVote = sessionStorage.getItem(voteCardContent.id);
        if(storageVote) {
            this.setState({ voteCount: parseInt(storageVote) });
        }
    }

    buildDescriptionAndThanks(voteSuccess) {
        const { voteCardContent, voteSectionTexts } = this.props;
        const descriptionContent = voteSuccess ? voteSectionTexts.thanksForVoting : voteCardContent.description;

        return (
            <div className="vote-card__description">
                <p>
                    {descriptionContent}
                </p>
            </div>
        )
    }

    handleVoteButton() {
        const { voteSelected } = this.state;
        const { voteCardContent } = this.props;

        if(this.state.voteDone) {
            this.setState({ voteDone: false, voteSelected: '' });
        }

        if(voteSelected === 'up' && this.state.voteCount <= 99 && !this.state.voteDone) {
            let voteCount = this.state.voteCount + 1;
            this.setState({ voteCount: voteCount, voteDone: true });
            sessionStorage.setItem(voteCardContent.id, voteCount);
        } else if (voteSelected === 'down' && this.state.voteCount >= 1 && !this.state.voteDone) {
            let voteCount = this.state.voteCount - 1;
            this.setState({ voteCount: voteCount, voteDone: true });
            sessionStorage.setItem(voteCardContent.id, voteCount);
        }
    }

    buildVoteButton(voteSuccess) {
        const { voteSectionTexts } = this.props;
        const buttonText = voteSuccess ? voteSectionTexts.voteAgain : voteSectionTexts.voteNow;
        return (
            <TransparentButton
                name={buttonText}
                handleOnClick={this.handleVoteButton}
                disabled={!this.state.voteSelected}
            />
        )
    }

    handleThumbVote(voteSelection) {
        this.setState({ voteSelected: voteSelection });
    }

    buildVoteThumbButton(voteSelection) {
        const voteThumbClass = voteSelection === 'up' ? 'vote-card__button' : 'vote-card__button vote-card__button--right';
        if(!this.state.voteDone) {
            return (
                <button
                    onClick={() => this.handleThumbVote(voteSelection)}
                    className={voteThumbClass}>
                    <VoteUpDownThumb
                        voteUp={voteSelection === 'up' ? true : false}
                        customClass={'vote-card__title-zone'}
                        hasBorder={this.state.voteSelected === voteSelection}
                    />
                </button>
            );
        }
    }

    render() {
        const { voteCardContent } = this.props;
        const source = images(voteCardContent.photo);
        return (
            <div className="vote-card" style={{ backgroundImage: `url(${source})` }}>
                <div className="vote-card__content">
                    <div>
                        <div className="vote-card__title-zone vote-card__title-zone--thumb">
                            <VoteUpDownThumb
                                voteUp={this.state.voteCount > 49}
                                customClass={'vote-card__title-zone'}
                            />
                        </div>
                        <h3 className="vote-card__title-zone vote-card__title-zone--celebrity-name">
                            {voteCardContent.name}
                        </h3>
                    </div>
                    <div className="vote-card__buttons-area">
                        <p className="vote-card__time-area">
                            <b>{voteCardContent.time}</b> {voteCardContent.workArea}
                        </p>
                        {this.buildDescriptionAndThanks(false)}
                        <div className="vote-card__vote-zone">
                            {this.buildVoteThumbButton('up')}
                            {this.buildVoteThumbButton('down')}
                            {this.buildVoteButton(this.state.voteDone)}
                        </div>
                    </div>
                    <VoteBar
                        numberOfvotesUp={this.state.voteCount}
                    />
                </div>
            </div>
        );
    }
};

export default VoteCard;