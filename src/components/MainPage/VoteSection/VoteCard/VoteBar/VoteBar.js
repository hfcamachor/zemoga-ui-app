import './VoteBar.scss';

import React from 'react';

// @assets
import thumbUp from 'assets/thumb-up.png';
import thumbDown from 'assets/thumb-down.png';

const VoteBar = (props) => {
    const { numberOfvotesUp } = props;
    const numberOfvotesDown = 100 - numberOfvotesUp;
    return (
        <div className="vote-bar">
            <div className="vote-bar__votes vote-bar__votes--up" style={{ width: `${numberOfvotesUp}%` }}>

            </div>
            <div className="vote-bar__votes vote-bar__votes--down" style={{ width: `${numberOfvotesDown}%` }}>

            </div>
            <div className="vote-bar__percentage-counter">
                <div className="vote-bar__counter">
                    <img className="vote-bar__count vote-bar__count--thumb" src={thumbUp} alt="Vote Down" />
                    <p className="vote-bar__count vote-bar__count--number">{numberOfvotesUp}%</p>
                </div>
                <div className="vote-bar__counter vote-bar__counter--right">
                    <p className="vote-bar__count vote-bar__count--number">{numberOfvotesDown}%</p>
                    <img className="vote-bar__count vote-bar__count--thumb" src={thumbDown} alt="Vote Down" />
                </div>
            </div>
        </div>
    )
}

export default VoteBar;