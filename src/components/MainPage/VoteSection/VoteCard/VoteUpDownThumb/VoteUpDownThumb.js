import './VoteUpDownThumb.scss';

import React from 'react';

// @assets
import thumbUp from 'assets/thumb-up.png';
import thumbDown from 'assets/thumb-down.png';

const VoteUpDownThumb = (props) => {
    const { voteUp, customClass, hasBorder } = props;
    const thumbImg = voteUp ? <img src={thumbUp} alt="Vote up" /> : <img src={thumbDown} alt="Vote Down" />;
    const voteClassName = voteUp ?
        'Vote-up-down-thumb--up' :
        'Vote-up-down-thumb--down';
    const borderVoteBox = hasBorder && 'Vote-up-down-thumb--border';
    return (
        <div className={`Vote-up-down-thumb ${voteClassName} ${customClass} ${borderVoteBox}`}>
            {thumbImg}
        </div>
    )
}

export default VoteUpDownThumb;
