import './VoteSection.scss';

import React, { Component } from 'react';
// @constans
import { voteSectionTexts, voteCardsCelebritiesCards } from '../../../constants/constants';

// @Components 
import VoteCard from './VoteCard/VoteCard';

class VoteSection extends Component {

    buildVoteCards() {
        const voteCards = voteCardsCelebritiesCards.map(card => (
            <div key={card.id}>
                <VoteCard
                    voteCardContent={card}
                    voteSectionTexts={voteSectionTexts}
                />
            </div>
        ))
        return voteCards;
    }

    render() {
        return (
            <div className="vote-section">
                <h2 className="vote-section__title">{voteSectionTexts.title}</h2>
                <div className="vote-section__container">
                    {this.buildVoteCards()}
                </div>
            </div>
        )
    }
}

export default VoteSection;