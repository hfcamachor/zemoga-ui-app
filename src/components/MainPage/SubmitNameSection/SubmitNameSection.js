import './SubmitNameSection.scss';

import React from 'react';

// @constants
import { submitNameSection } from 'constants/constants';

// @components
import TransparentButton from 'components/commons/TransparentButton/TransparentButton';

const SubmitNameSection = () => (
    <div className="submit-name">
        <div className="submit-name__section submit-name__section--description">
            <p>{submitNameSection.description}</p>
        </div>
        <div className="submit-name__section submit-name__section--button">
            <TransparentButton
                name={submitNameSection.buttonName}
                color='black'
                size='big'
            />
        </div>
    </div>
);

export default SubmitNameSection;