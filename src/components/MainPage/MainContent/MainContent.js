import './MainContent.scss';

import React from 'react';
// @components
import MainVoteBox from './MainVoteBox/MainVoteBox';
// @constants
import { mainSectionTexts } from './../../../constants/constants';


const MainContent = ()  => (
    <div className="main-content">
        <MainVoteBox />
        <div className="main-content__closing-in">
            <div className="main-content__closing-in-item main-content__closing-in-item--left">
                <p>{mainSectionTexts.closingIn}</p>
            </div>
            <div className="main-content__closing-in-item main-content__closing-in-item--right">
                <p><b>{mainSectionTexts.daysToClose}</b> {mainSectionTexts.days}</p>
            </div>
        </div>
    </div>
)

export default MainContent;