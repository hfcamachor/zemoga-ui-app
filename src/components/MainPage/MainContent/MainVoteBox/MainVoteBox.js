import './MainVoteBox.scss';

import React from 'react';
 // @constants
 import { mainSectionTexts } from './../../../../constants/constants';
 
 // @assets
import thumbUp from '../../../../assets/thumb-up.png';
import thumbDown from '../../../../assets/thumb-down.png';

const MainVoteBox = () => (
    <div className="main-vote-box">
        <div className="main-vote-box__photo"></div>
        <div className="main-vote-box__description">
            <h1 className="main-vote-box__title">
                <span className="main-vote-box__title--small">
                    {mainSectionTexts.titlePartOne}
                </span>
                {mainSectionTexts.titlePartTwo}
            </h1>
            <p className="main-vote-box__vote-description">
                {mainSectionTexts.mainVoteSectionDescription}
            </p>
            <span className="main-vote-box__wiki-logo"></span>
            <a href="#" className="main-vote-box__more-info-link">
                {mainSectionTexts.mainVoteMoreInformation}
            </a>
            <p className="main-vote-box__veredict">
                {mainSectionTexts.mainVoteVeredict}
            </p>
        </div>
        <div className="main-vote-box__vote-section">
            <div className="main-vote-box__vote-section__box main-vote-box__vote-section__box--up">
                <button className="main-vote-box__vote-button main-vote-box__vote-button--up">
                    <img src={thumbUp} alt="vote up" />
                </button>
            </div>
            <div className="main-vote-box__vote-section__box main-vote-box__vote-section__box--down">
                <button className="main-vote-box__vote-button main-vote-box__vote-button--down">
                    <img src={thumbDown} alt="vote down" />
                </button>
            </div>
        </div>
    </div>
)

export default MainVoteBox;