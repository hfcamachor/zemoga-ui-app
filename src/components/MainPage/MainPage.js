import './MainPage.scss';

import React, { Component } from 'react';

// @Components
import MainContent from './MainContent/MainContent';
import InfoSection from './InfoSection/InfoSection';
import VoteSection from './VoteSection/VoteSection';
import SubmitNameSection from './SubmitNameSection/SubmitNameSection';

class MainPage extends Component {
    render() {
        return (
            <div className="main-page">
                <MainContent />
                <div className="main-page__container">
                    <InfoSection />
                    <VoteSection />
                    <SubmitNameSection />
                </div>
            </div>
        )
    }
}

export default MainPage;