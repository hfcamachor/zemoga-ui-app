import './InfoSection.scss';

import React from 'react';
import closeIcon from 'assets/close-icon.png';

// @constants
import { infoSection } from 'constants/constants';

const InfoSection = () => (
    <div className="info-section">
        <div className="info-section__part info-section__title">
            {infoSection.titlePartOne}
            <span className="info-section__title--part-two">
                {infoSection.titlePartTwo}
            </span>
        </div>
        <div className="info-section__part info-section__description">
            {infoSection.description}
        </div>
        <div className="info-section__part info-section__close">
            <img className="vote-bar__count vote-bar__count--thumb" src={closeIcon} alt="Vote Down" />
        </div>
    </div>
);

export default InfoSection;