import './Footer.scss';

import React from 'react';

// @constants
import { footerSection } from 'constants/constants';

 // @assets
 import twitter from 'assets/icon-twiter.png';
 import facebook from 'assets/icon-facebook.png';

const buildLinksSection = () => {
    const links = Object.keys(footerSection.links).map((link, index) => {
        return (
            <li className="footer-nav__links" key={index}>
                <a href="#">
                    {footerSection.links[link]}
                </a>
            </li>
        )
    })
    return links
}

const Footer = () => (
    <div className="footer-nav">
        <div className="footer-nav__section footer-nav__section--link">
            <ul>
                {buildLinksSection()}
            </ul>
        </div>
        <div className="footer-nav__section footer-nav__section--social">
            <p className="footer-nav__social">
                {footerSection.followUs}
            </p>
            <button className="footer-nav__social footer-nav__social--button">
                <img src={twitter} alt="Twitter" />
            </button>
            <button className="footer-nav__social footer-nav__social--button">
                <img src={facebook} alt="Facebook" />
            </button>
        </div>
    </div>
);

export default Footer;