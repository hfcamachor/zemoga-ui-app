import './TransparentButton.scss';

import React from 'react';

const TransparentButton = (props) => {
    const { name, color, size, disabled, handleOnClick } = props;
    const colorClass = color === 'black' ? 'transparent-button--black' : '';
    const sizeClass = size === 'big' ? 'transparent-button--big' : '';
    return (
        <button 
            className={`transparent-button ${colorClass} ${sizeClass}`}
            onClick={() => handleOnClick && handleOnClick()}
            disabled={disabled}>
            {name}
        </button>
    )
}

export default TransparentButton;