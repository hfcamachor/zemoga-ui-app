import './NavBar.scss';

import React, { Component } from 'react'
import { NavLink } from 'react-router-dom';
// @constants
import { navButtonsTexts } from '../../constants/constants';
// @assets
import hamburguerIcon from '../../assets/menu-icon.png';

class NavBar extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isMenuOpen: false
        }

        this.handleMenuState = this.handleMenuState.bind(this);
    }

    buildNavLinks() {
        const navLinks = Object.keys(navButtonsTexts).map((liItem, index) => {
            return (
                <li className="nav-bar__item" key={index}>
                    <NavLink className="nav-bar__link" to={navButtonsTexts[liItem].linkTo} exact>
                        {navButtonsTexts[liItem].name}
                    </NavLink>
                </li>
            )
        })
        return navLinks;
    }

    handleMenuState() {
        const menuState = !this.state.isMenuOpen;
        this.setState({ isMenuOpen: menuState })
    }

    render() {
        const menuClass = this.state.isMenuOpen ? 'nav-bar--open' : '';
        const menuIcon = this.state.isMenuOpen ? 'x' : <img src={hamburguerIcon} alt="menu" />;
        return (
            <ul className={`nav-bar ${menuClass}`}>
                <button
                    className="nav-bar__close"
                    onClick={() => this.handleMenuState()}
                >
                    {menuIcon}
                </button>
                {this.buildNavLinks()}
                <div className="nav-bar__search"></div>
            </ul>
        )
    }
}

export default NavBar;