const navButtonsTexts = {
    home: {
        name: 'Home',
        linkTo: '/'
    },
    pastTrials: {
        name: 'Past Trials',
        linkTo: '/past-trials'
    },
    howItWorks: {
        name: 'How It Works',
        linkTo: '/how-it-works'
    },
    logSignUp: {
        name: 'Log In / Sign Up',
        linkTo: '/log-in-sign-up'
    }
}

const mainSectionTexts = {
    titlePartOne: 'What’s your opinion on',
    titlePartTwo: 'Pope Francis?',
    mainVoteSectionDescription: 'He’s talking tough on clergy sexual abuse, but is he just another papal pervert protector? (thumbs down) or a true pedophile punishing pontiff? (thumbs up)',
    mainVoteMoreInformation: 'More information',
    mainVoteVeredict: 'What’s Your Verdict?',
    closingIn: 'CLOSING IN',
    daysToClose: '22',    
    days: 'days',    
}

const infoSection = {
    titlePartOne: 'Speak out. Be heard.',
    titlePartTwo: 'Be counted',
    description: 'Rule of Thumb is a crowd sourced court of public opinion where anyone and everyone can speak out and speak freely. It’s easy: You share your opinion, we analyze and put the data in a public report.'
}

const voteSectionTexts = {
    title: 'Votes',
    voteNow: 'Vote now',
    voteAgain: 'Vote again',
    thanksForVoting: 'Thank you for voting!'
}

const voteCardsCelebritiesCards = [
    {
        name: 'Kanye West',
        id: 'kanyeWest',
        photo: './kanye-photo.jpg',
        description: 'Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.',
        time: '1 month ago',
        workArea: 'in Entertainment'
    },
    {
        name: 'Mark Zuckerberg',
        id: 'markZuckerberg',
        photo: './mark-photo.jpg',
        description: 'Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.',
        time: '1 month ago',
        workArea: 'in Business'
    },
    {
        name: 'Cristina Fernández de Kirchner',
        id: 'cristinaFernandezDeKirchner',
        photo: './cristina-photo.jpg',
        description: 'Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.',
        time: '1 month ago',
        workArea: 'in Politics'
    },
    {
        name: 'Malala Yousafzai',
        id: 'malalaYousafzai',
        photo: './malala-photo.jpg',
        description: 'Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.',
        time: '1 month ago',
        workArea: 'in Entertainment'
    },
]

const submitNameSection = {
    description: 'Is there anyone else you would want us to add?',
    buttonName: 'Submit a Name'
}

const footerSection = {
    links: {
        terms: 'Terms and Conditions',
        privacyPolicy: 'Privacy Policy',
        contactUs: 'Contact Us'
    },
    followUs: 'Follow Us'
}

export {
    navButtonsTexts,
    mainSectionTexts,
    voteSectionTexts,
    voteCardsCelebritiesCards,
    infoSection,
    submitNameSection,
    footerSection
}